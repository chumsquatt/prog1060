

import arcpy
import csv


#GDB paths
potholes_template = "C:\\GIS\\scratch.gdb\\potholes_template"
root = "C:\\GIS\\scratch.gdb"
potholes = "C:\\GIS\\scratch.gdb\\potholes_2"

#Data path
CSV_PATH = "C:\\Users\\User\\PycharmProjects\\arcpy_suite\\pothole_data_2.csv"

#Spatial reference object for the Create Features tool
WGS84 = arcpy.SpatialReference(4326)


def main():
    """
    Loads data produced from the Accelboy Arduino script and the circuit with modules and charges the data to a
    feature class of point objects. Although I had planned for additional processing, the data is simply too unreliable
    and spotty to make it worthwhile, even for pretend.

    :return:
    """

    print("Preparing workspace...")
    if arcpy.Exists(potholes):
        arcpy.Delete_management(potholes)
    arcpy.CreateFeatureclass_management(root,"potholes_2","POINT",potholes_template,"DISABLED","DISABLED",WGS84,"","0","0","0")

    inserto = arcpy.da.InsertCursor(potholes, ["SHAPE@","INTENSITY"])

    print("Writing data to feature class...")
    with open(CSV_PATH, "r") as fb:

        first = True

        #Create iterable object
        reader = csv.reader(fb)

        #Iterate the iterable, making sure to convert the integer longitude and latitude into floats
        for row in reader:
            if not first:
                event_location = arcpy.Point(float(row[0])/1000000.0, float(row[1])/1000000.0)
                event_intensity = abs(int(row[2]))
                inserto.insertRow([event_location, event_intensity])
            else:
                first = False

    del inserto

    return




main()