
import arcpy
import urllib2
import json

# PARAMETERS
YEAR = str(2017)
STATE_CODE = 49
COORDS = (-111.892594, 40.770556) #lng, lat
RADII = [3.0,1.0,0.5]
SPATIAL_REFERENCE = arcpy.SpatialReference(4269) #NAD1983

#Census API call, must be formatted: note, county code comes out as string
PATH = "https://api.census.gov/data/{:s}/acs/acs5?get=B01001_001E&for=block%20group:*&in=state:{:02d}&in=county:".format(YEAR, STATE_CODE)
# Append in the code: {:03s}&in=tract:*
#FIXME! In future, add additional demographic categories

# GIS paths:
site_location = "C:\\GIS\\scratch.gdb\\site_loc"
site_loc_buffer = "C:\\GIS\\scratch.gdb\\site_loc_buffer"
census_blocks = "C:\\GIS\\state_block_shapefiles\\tl_2017_49_tabblock10.shp"
block_groups = "C:\\GIS\\scratch.gdb\\block_groups"
buffer_bg_intersect = "C:\\GIS\\scratch.gdb\\buffer_bg_intersect"


def retrieve_census_data(bg_list):
    """

    Note: JSON results: [0] pop, [1] state [2] county [3] tract (no periods] [4] blockgroup

    :param bg_list: the list of blockgroups
    :return: dataset, a dictionary of integers: Key = String {:3s}{:6s}{:1s}. Value = population
    """

    data_set = {}

    #Get list of counties, as these are required for api call
    #Get list of ids, for lookup
    county_list = set()
    groups_in_radius = set()


    #Dismembers the group id and defines county and tract codes for the API call
    for group in bg_list:
        county_list.add(group[0])
        groups_in_radius.add(str(group[0]) + str(group[1]) + str(group[2]))

    #For each COUNTY CODE excised from the blockgroup list, runs through the group signatures (county, tract, group
    for county_code in county_list:
        path = PATH + county_code + "&in=tract:*"

        try:
            request = urllib2.Request(path)
            opener = urllib2.urlopen(request)
            results = opener.read()

            county_groups = json.loads(results)

            for group in county_groups:
                group_signature = str(group[2]) + str(group[3]).replace(".","") + str(group[4])[0]
                if group_signature in groups_in_radius:
                    data_set[group_signature] = int(group[0])

        except:
            print("Couldn't load Census data.")



    return data_set



def get_blocklists():
    """
    Retrieves groups in the radius of the site, using the arcpy Search Cursor

    :return:  a list of tuples: [0]county, [1]tract, [2]block
    """

    groups_in_range = []

    with arcpy.da.SearchCursor(buffer_bg_intersect,["BG_CODE"])as sc:

        #BG_CODE: county-[3], tract- [6] ,group-[1]

        for row in sc:
            groups_in_range.append((row[0][-10:-7],row[0][-7:-1],row[0][-1:]))


    return groups_in_range



def generate_block_groups():
    """
    Resolves blockgroups from individual blocks. Block groups are defined simply as all blocks that share the initial number
    of the block id. They also share county and tract number.

    :return: Produces ArcGIS entities
    """

    # Process: Add Field

    #Delete field, if exists
    try:
        arcpy.DeleteField_management(census_blocks, "BG_CODE")
    except:
        pass

    arcpy.AddField_management(census_blocks, "BG_CODE", "TEXT", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")

    # Calculate Field to fill in the field from the existing fields
    arcpy.CalculateField_management(census_blocks, "BG_CODE", "str(!COUNTYFP10!) + str( !TRACTCE10!).replace('.','') + str( !BLOCKCE10!)[0]", "PYTHON", "")

    # Run the dissolve tool to create a map object of objects that share the value in the new blockgroup field
    if arcpy.Exists(block_groups):
        arcpy.Delete_management(block_groups)

    arcpy.Dissolve_management(census_blocks, block_groups, "BG_CODE", "", "MULTI_PART", "DISSOLVE_LINES")


    return


def main():
    """
    Blockboy determines all the census blockgroups within a given radius of a given set of coordinates, then uses the
    Census API to retrieve population data about those groups, and produces an aggregate figure. This could be expanded
    to include other demographics.

    :return:
    """

    print("Processing geodatabase...")
    #Check old coords with the coords above
    old_coords = []
    with arcpy.da.SearchCursor(site_location, ['SHAPE@XY'], "",SPATIAL_REFERENCE) as sc:
        for item in sc:
            old_coords.append(item[0][0])
            old_coords.append(item[0][1])

    print("Updating coordinates...")
    if old_coords[0] != COORDS[0] or old_coords[1] != COORDS[0]:
        sql = "OBJECTID = 1"
        with arcpy.da.UpdateCursor(site_location, ['OID@','SHAPE@XY'], sql,SPATIAL_REFERENCE) as uc:

            for row in uc:
                row[1] = COORDS
                uc.updateRow(row)

    print("Distilling census block groups...")
    generate_block_groups()

    #The loop for the different radii in the radii list parameter
    for radius in RADII:
        # Process: Buffer
        print("Buffering location...")
        if arcpy.Exists(site_loc_buffer):
            arcpy.Delete_management(site_loc_buffer)

        arcpy.Buffer_analysis(site_location, site_loc_buffer, str(radius)+" Miles", "FULL", "ROUND", "NONE", "", "PLANAR")


        # Calls the ArcGIS Intersect tool to merge radius buffer layer with blockgroups layer
        print("Performing group-radius intersection...")
        if arcpy.Exists(buffer_bg_intersect):
            arcpy.Delete_management(buffer_bg_intersect)

        arcpy.Intersect_analysis([site_loc_buffer, block_groups], buffer_bg_intersect, "ALL", "", "INPUT")


        if arcpy.Exists(buffer_bg_intersect):
            groups_in_range = get_blocklists()
        else:
            print("The intersection file does not exist.")


        data_set = retrieve_census_data(groups_in_range)

        #FIXME! In future, add something like: charge_data_to_geodatabase(data_set)


        print("Population within a {:.2f}-mile radius: ".format(radius), sum(data_set.values()))

    return

main()

