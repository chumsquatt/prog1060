
import arcpy
import time
import random

#Counties include in the raster data
COUNTIES = ["weber","davis","morgan", "cache","rich","salt lake", "box elder","tooele","utah","sanpete","wasatch","summit","duchesne"]
SELECTED = COUNTIES[5] #County to process
SELECTED_FORMATTED = SELECTED if not SELECTED.find(" ") else SELECTED.replace(" ", "_") #Special formatting for county name

#Accumulation threshold for the Find Nulls tool
STREAM_THRESHOLD = 5000

# Persistent layers
utah_elevs = "C:\\GIS\\streamomatic.gdb\\utah_elevs"
utah_counties = "C:\\GIS\\utah_counties\\Counties.shp"

# Temporary layers
county_layer = "C:\\GIS\\streamocatic.gdb\\county_layer"
county_extent = "C:\\GIS\\streamomatic.gdb\\county_extent"
county_elev = "C:\\GIS\\streamomatic.gdb\\county_elev"
county_flow_dir = "C:\\GIS\\streamomatic.gdb\\county_flow_dir"
county_flow_acc = "C:\\GIS\\streamomatic.gdb\\county_flow_acc"
county_stream_vals = "C:\\GIS\\streamomatic.gdb\\county_stream_vals"
county_stream_order = "C:\\GIS\\streamomatic.gdb\\county_stream_order"

# Output layer with random stamp
random.seed(time.time())
plug = random.randint(10000,99999)
county_stream_vector = "C:\\GIS\\streamomatic.gdb\\"+ SELECTED_FORMATTED + "_stream_vector_" + str(plug)

#Simple Exception class
class BadBoy(Exception):
    pass


#A series of conditional checks and deletions
def delete_temporary_files():
    if arcpy.Exists(county_layer):
        arcpy.Delete_management(county_layer)
    if arcpy.Exists(county_elev):
        arcpy.Delete_management(county_elev)
    if arcpy.Exists(county_extent):
        arcpy.Delete_management(county_extent)
    if arcpy.Exists(county_flow_acc):
        arcpy.Delete_management(county_flow_acc)
    if arcpy.Exists(county_flow_dir):
        arcpy.Delete_management(county_flow_dir)
    if arcpy.Exists(county_stream_vals):
        arcpy.Delete_management(county_stream_vals)
    if arcpy.Exists(county_stream_order):
        arcpy.Delete_management(county_stream_order)
    return


def main():
    """
    Streamomatic is a "standard" use-case script: starting from a DEM raster, and using a linked chain of
    ArcGIS hydrology spatial analysis tools, discovering streams and streambeds in an area. Outputs to a
    vector feature class.

    :return:
    """


    in_time = time.clock() #For debuggery and general interest, clock the process length

    #Check for license
    try:
        if arcpy.CheckExtension("Spatial") == "Available":
            arcpy.CheckOutExtension("Spatial")
        else:
            raise BadBoy

        print("Initializing workspace...")

        while True:
            try:
                delete_temporary_files()
                break
            except:
                try:
                    input("One or more files is open in another program. Close the program and press enter")
                except:
                    pass


        # 3-part process, not necessarily very efficient:
        #Select county into layer, clip raster based on extent of the county, and extract shape of county by using
        #the county layer as a mask
        print("Extracting " + SELECTED.capitalize() + " county...")

        #1. make selection layer/Mask
        arcpy.MakeFeatureLayer_management(utah_counties, county_layer, "\"NAME\" = '" + SELECTED.upper() + "'", "", "FID FID VISIBLE NONE;Shape Shape VISIBLE NONE;COUNTYNBR COUNTYNBR VISIBLE NONE;ENTITYNBR ENTITYNBR VISIBLE NONE;ENTITYYR ENTITYYR VISIBLE NONE;NAME NAME VISIBLE NONE;FIPS FIPS VISIBLE NONE;STATEPLANE STATEPLANE VISIBLE NONE;POP_LASTCE POP_LASTCE VISIBLE NONE;POP_CURRES POP_CURRES VISIBLE NONE;FIPS_STR FIPS_STR VISIBLE NONE;COLOR4 COLOR4 VISIBLE NONE;Shape_Leng Shape_Leng VISIBLE NONE;Shape_Area Shape_Area VISIBLE NONE")

        #2.a get extent directly from counties shapefile
        with arcpy.da.SearchCursor(utah_counties, ["SHAPE@", "NAME"]) as sc:

            for row in sc:
                if row[1] == SELECTED.upper():
                    extent = row[0].extent

        extent_string = "{:.2f} {:.2f} {:.2f} {:.2f}".format(extent.XMin, extent.YMin, extent.XMax, extent.YMax)

        #2.b Clip raster with county extent
        print("Retrieving county elevations...")
        arcpy.Clip_management(utah_elevs, extent_string, county_extent, "", "-3.402823e+038", "NONE", "NO_MAINTAIN_EXTENT")

        #3. Extract the selected county shape from the clipped raster
        arcpy.gp.ExtractByMask_sa(utah_elevs, county_layer, county_elev)


        # GeoProcessing: Flow Direction
        print("Calculating flow direction...")
        arcpy.gp.FlowDirection_sa(county_elev, county_flow_dir, "NORMAL", "")

        # GeoProcessing: Flow Accumulation
        print("Calculating flow accumulations...")
        arcpy.gp.FlowAccumulation_sa(county_flow_dir, county_flow_acc, "", "FLOAT")

        # Raster processing: Set to null all cells with low acculations
        print("Defining streams...")
        arcpy.gp.SetNull_sa(county_flow_acc, county_flow_acc, county_stream_vals, "Value < " + str(STREAM_THRESHOLD))

        # Geoprocessing: Calculate stream order
        print("Calculating stream order...")
        arcpy.gp.StreamOrder_sa(county_stream_vals, county_flow_dir, county_stream_order, "STRAHLER")

        # Formatting: Convert stream raster to vector
        print("Converting streams from raster to vector...")
        arcpy.gp.StreamToFeature_sa(county_stream_order, county_flow_dir, county_stream_vector, "SIMPLIFY")

        print("Cleaning residual files...")
        while True:
            try:
                delete_temporary_files()
                break
            except:
                try:
                    input("One or more files is open in another program. Close the program and press enter")
                except:
                    pass

    except:
        print("You do not have the Spatial Analysis license")


    print("Created file: " + county_stream_order)
    out_time = time.clock()
    print("Processing time: " + str(out_time - in_time))

    return


main()
