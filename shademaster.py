
# Import arcpy module and skyfield
import arcpy
from skyfield.api import load, Topos

# Geodatabase paths:
root = "C:\\GIS\\scratch.gdb"
ogden_trails = "C:\\GIS\\scratch.gdb\\ogden_trails"
trail_elev = "C:\\GIS\\scratch.gdb\\n42w112_10m_Clip"
temp_shade_raster = "C:\\GIS\\scratch.gdb\\temp_shade_raster"
temp_shade_table = "C:\\GIS\\scratch.gdb\\temp_shade_table"

#Output paths
shademaster_temp = "C:\\GIS\\scratch.gdb\\shademaster_temp" #template for the output
shademaster_output = "C:\\GIS\\scratch.gdb\\shademaster_output"

#The coordinates of the 27th St. Trailhead in Ogden
SITE_LAT = '41.215837 N'
SITE_LNG = '-111.929079 W'
UTC_FROM_TZ = 6 #UTC from observation timezone, in tihs case, MST; No daylight savings; 7, march to november

#Astronomical constants
SOLAR_SYSTEM = load('de421.bsp') #load positioning data
TERRA = SOLAR_SYSTEM['earth'] #load earth position
SOL = SOLAR_SYSTEM['sun'] #load sun position

#If desired, a basic filter for the hillshade, 0 is 100% shade, 254 is 0% shade
THRESHOLD = 254.0

#Basic exception class
class BadBoy(Exception):
    pass


def get_alt_az(ogden, timeboy):
    """
    Uses skyfield to get the azimuth and altitude of the sun at the given coordinates.

    :param ogden:
    :param timeboy:
    :return: tuple [0]alt, [1] az
    """


    astro_pos = ogden.at(timeboy).observe(SOL) #The sun, observed from the given position at the given time
    appearance = astro_pos.apparent() #The apparent position

    altitude, azimuth, distance = appearance.altaz()

    #A hack of  the altitude reading, to prepare for the ArcGIS geoprocessing tool
    reading = str(altitude).split(" ")
    deg = float(reading[0][:-3])
    min = float(float(reading[1][:-1])/60)
    sec = float(float(reading[2][:-1])/3600)
    alt_dec_deg = deg + min + sec if deg > 0 else deg - min - sec

    if alt_dec_deg < 0:
        alt_dec_deg = 0.0

    #Prepare azimuth reading for the geoprocessing tool
    reading = str(azimuth).split(" ")
    deg = float(reading[0][:-3])
    min = float(float(reading[1][:-1])/60)
    sec = float(float(reading[2][:-1])/3600)
    az_dec_deg = deg + min + sec if deg > 0 else deg - min - sec

    if az_dec_deg < 0:
        az_dec_deg = 360 - az_dec_deg

    #returns a tuple
    return alt_dec_deg, az_dec_deg


def get_times_array():
    """
    Produce an array of dates and times for which to retrieve azimuths and altitudes
    :return: a list of times to plug into skyfield
    """

    timescale = load.timescale()
    times_array = list()

    for monthboy in range(1,2):
        for dayboy in [1,15]:
            for hourboy in range(6,11):
                times_array.append(timescale.utc(2018,monthboy,dayboy, hourboy - UTC_FROM_TZ))

    return times_array


def clean_residue():
    """
    Cleans up temporary ArcGIS entities
    :return:
    """
    if arcpy.Exists(temp_shade_raster):
        arcpy.Delete_management(temp_shade_raster)
    if arcpy.Exists(temp_shade_table):
        arcpy.Delete_management(temp_shade_table)

    return


def get_date_hour(timeboy_str):
    """
    Extracts simplified date string from timeboy, as well as an hour, which is converted by to local time

    :param timeboy_str:
    :return:
    """

    split_one = timeboy_str.split("-")
    split_two = split_one[2].split(":")

    year = split_one[0].strip()
    month = split_one[1].strip()
    day = split_one[2][0:2]
    hour = (int(split_two[0][-2:]) + UTC_FROM_TZ) % 24
    return year +"-"+month+"-"+day, hour


def main():
    """
    Shademaster finds the hillshade at a particular place at a series of times, using an ArcGIS hillshade geoprocessing tool.
    Outputs an ArcGIS table
    :return:
    """
    #Initial check for Spatial Analyst license
    try:
        if arcpy.CheckExtension("Spatial") == "Available":
            arcpy.CheckOutExtension("Spatial")
        else:
            raise BadBoy

        # [0]time_string, [1] min shade, [2]max shade, [3] mean shade]
        shade_data = []

        ogden = TERRA + Topos(SITE_LAT, SITE_LNG) #create Topological opbject
        print("Retrieving time range...")
        times_array = get_times_array() #get times

        #Main loop: cycles through the times of interst (12 months, beginning and middle, 4am to 12pm)
        #Each loops, gets altitude and azimuth, to plug into arcpy hillshade.

        for timeboy in times_array:

            print("Calculating altitude and azimuth for " + timeboy.utc_iso())
            alt, azi = get_alt_az(ogden, timeboy)
            print("alt, az: ", alt,azi)


            print("Generating hillshade raster...")
            # Process: Hillshade
            if arcpy.Exists(temp_shade_raster):
                arcpy.Delete_management(temp_shade_raster)
            arcpy.gp.HillShade_sa(trail_elev, temp_shade_raster, str(azi), str(alt), "SHADOWS", "1")

            print("Processing hillshade statistics...")
            # Process: Zonal Statistics as Table
            if arcpy.Exists(temp_shade_table):
                arcpy.Delete_management(temp_shade_table)
            arcpy.gp.ZonalStatisticsAsTable_sa(ogden_trails, "SEGMENT", temp_shade_raster, temp_shade_table, "DATA", "MIN_MAX_MEAN")

            #Searchcursor
            print("Aggregating statistics...")
            with arcpy.da.SearchCursor(temp_shade_table, ["MIN","MAX","MEAN","SEGMENT"]) as sc:
                for row in sc:

                    #tuple: [0] date str, [1] hour int
                    date_hour = get_date_hour(str(timeboy.utc_iso()))



                    shade_data.append([date_hour[0], date_hour[1], row[3], row[0],row[1],row[2]])

            print(" ")

        print("Producing table...")

        #A simple form of exception handling: allows user to free file without having to recalculate
        while True:
            try:
                if arcpy.Exists(shademaster_output):
                    arcpy.Delete_management(shademaster_output)
                arcpy.CreateTable_management(root, "shademaster_output", shademaster_temp, "")
                break
            except:
                try:
                    input("Existing output file is being used. Close file and press enter.")
                except:
                    pass

        ic = arcpy.da.InsertCursor(shademaster_output, ["DATE","HOUR","SEGMENT","MIN_SHADE", "MAX_SHADE","MEAN_SHADE"])
        for row in shade_data:
            if row[5] <THRESHOLD:
                ic.insertRow(row)
        del ic

        print("Cleaning up...")

        clean_residue()
    except BadBoy:
        print("You don't have what it takes.")
    return



main()
