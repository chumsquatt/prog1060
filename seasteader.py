

import arcpy
import random


# GDB paths
root = "C:\\GIS\\scratch.gdb"
template = "C:\\GIS\\scratch.gdb\\houseboat_temp" #day[int], boat_num [int], boat_name [text], move_prob[float], production [int], cluster [text]
houseboats = "C:\\GIS\\scratch.gdb\\houseboats"
day_clusters = "C:\\GIS\\scratch.gdb\\day_clusters"
houseboat_day_layer = "C:\\GIS\\scratch.gdb\\houseboat_day_layer"
boat_paths = "C:\\GIS\\scratch.gdb\\boat_paths"
path_temp = "C:\\GIS\\scratch.gdb\\path_template"

#Seastead extent
NORTH = 42.086883 #lat_max y
SOUTH = 41.910565 #lat_min y
EAST = -111.292337 #lng_max x
WEST = -111.380139 #lng_min x

#Range as Int; divide by 10000000.0 and add to lat_min or lng_min
LAT_RANGE = int((NORTH-SOUTH) * 1000000)
LNG_RANGE = int((EAST-WEST) * 1000000)

#***** Simulation Parameters ***********
NUM_BOATS = 50
STARTING_CLUSTERS = 4
MAX_PRODUCTION = 100
MAX_CHANCE = 0.75
MIN_CHANCE = 0.25
PERIOD = 10 #days
#****************************************

#Quick way to break out of the cluster calculation, without a chain of checkpoints
class Nope(Exception):
    print("Something went wrong during the cluster calculation")
    pass

#Holder of boat information across days of the simulation
class Houseboat:

    def __init__(self, point, day, boat_num, boat_name, move_prob, production, boat_x, boat_y):
        self.point = point
        self.day = day
        self.boat_num = boat_num
        self.boat_name = boat_name
        self.move_prob = move_prob
        self.production = production

        self.associates = self.__init_associates()
        self.path = [point]

        return

    #Returns a dictionary counting number of times each boat was in cluster
    def __init_associates(self):

        tally = {}
        for boat in range(1, NUM_BOATS+1):
            tally[boat] = 0

        return tally

    def increment_associates(self, boat_id):
        self.associates[boat_id] += 1
        return

    def add_to_path(self, point):
        self.path.append(point)
        return

    def get_path(self):
        return self.path

    def discover_allies(self):
        """
        Presupposes that the associates dictionary attribute has been completely filled. Proceeds through the dictionary
        and selects the top 5.

        :return: a list of tuples: [0] boat_id, [1] number of instances/PERIOD
        """

        max_five = [0,0,0,0,0]
        boat_ids = [0,0,0,0,0]
        allies_list = []

        for boat in range(1, NUM_BOATS+1):

            for place in range(0,5):
                if self.associates.get(boat) > max_five[place]:
                    shiftval = max_five[place]
                    max_five[place] = self.associates.get(boat)

                    shiftkey = boat_ids[place]
                    boat_ids[place] = boat

                    for parser in range (place+1, 5):
                        temp = max_five[parser]
                        max_five[parser] = shiftval
                        shiftval = temp

                        temp = boat_ids[parser]
                        boat_ids[parser] = shiftkey
                        shiftkey = temp
                break

        for boat in range(0,5):
            allies_list.append((boat_ids[boat], max_five[boat]) )

        return allies_list

    def get_number(self):
        return self.boat_num


#Convenience methods
    def feed(self, point, day, move_prob, production):
        """
        Convenience method for writing to arcpy Cursor

        :param point:
        :param day:
        :param move_prob:
        :param production:
        :return:
        """
        self.point = point
        self.day = day
        self.move_prob = move_prob
        self.production = production
        return

    def pee(self):
        """
        Unloads location and move probability for the markov component
        :return:
        """
        return self.point, self.move_prob


    def poo(self):
        """
        Dumps list of all fields, for insertcursor
        :return:
        """
        return [self.point, self.day, self.boat_num, self.boat_name, self.move_prob, self.production,self.point.X, self.point.Y]



def generate_paths(houseboat_list):
    """
    Creates a feature class for polylines representing the positions of each boat over the course of the simulation.
    :param houseboat_list:
    :return:
    """
    path_list = []

    for boat in houseboat_list:

        temp_array = arcpy.Array() #build arcpy point array object
        temp_array.extend(boat.get_path()) #extend point array with list
        path = arcpy.Polyline(temp_array) #build polyline with point array
        path_list.append(path) #build list of polylines

    if arcpy.Exists(boat_paths):
        arcpy.Delete_management(boat_paths)
    arcpy.CreateFeatureclass_management(root, "boat_paths", "POLYLINE", path_temp, "DISABLED", "DISABLED", "GEOGCS['GCS_North_American_1983',DATUM['D_North_American_1983',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119521E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")

    ic = arcpy.da.InsertCursor(boat_paths, ["SHAPE@", "BOAT_NUM", "ALLIES"])
    for boat_num in range(0, len(houseboat_list)):
        ic.insertRow([path_list[boat_num],houseboat_list[boat_num].get_number(), str(houseboat_list[boat_num].discover_allies())])

    del ic

    return

def __update_associates(cluster_list, houseboat_list):
    for day in range(0, len(cluster_list)):

        for cluster in range(1,len(cluster_list[day].keys())+1):  #enumerating the SS_GROUPS
            cluster_members = cluster_list[day].get(cluster) #list

            for member in cluster_members: #boat_nums, starts from 1; houseboat_list starts from 0

                for index in range(0,len(cluster_members)): #this to tally each member's associates based on members list
                    if cluster_members[index] != member:
                        houseboat_list[member-1].increment_associates(cluster_members[index])
    return

def discover_clusters(houseboat_list):
    """
    Runs the arcpy Grouping Analysis tool on the houseboats list, using the x/y position as the grouping criterion.
    Note, this assumes that the simulation has already been run. The results of the grouping analysis do not directly
    affect the next step.

    :param houseboat_list:
    :return:
    """

    #list of SS_GROUP-keyed dictionaries, with boat_nums as contents
    cluster_list = []
    max_clusters = STARTING_CLUSTERS #stores the max number of clusters per any day in the period

    for day in range(1, PERIOD+1):

        num_clusters = STARTING_CLUSTERS
        #construct daily clusters lists: key = SS_GROUP, value = list of BOAT_NUM


        if arcpy.Exists(houseboat_day_layer):
            arcpy.Delete_management(houseboat_day_layer)
        arcpy.MakeFeatureLayer_management(houseboats, houseboat_day_layer,'"DAY"=' + str(day),"","")

        #SS_GROUP: this subroutine increments the number of groups to model for, based on requirements of algorithm
        while True:
            try:
                if arcpy.Exists(day_clusters):
                    arcpy.Delete_management(day_clusters)
                arcpy.GroupingAnalysis_stats(houseboat_day_layer, "BOAT_NUM", day_clusters, str(num_clusters), "BOAT_X;BOAT_Y", "K_NEAREST_NEIGHBORS", "EUCLIDEAN", "2", "", "FIND_SEED_LOCATIONS", "", "", "DO_NOT_EVALUATE")
                break
            except:
                num_clusters += num_clusters
                if num_clusters > max_clusters:
                    max_clusters = num_clusters
                if num_clusters > 15: #
                    raise Nope

        day_cluster_dict = {}


        with arcpy.da.SearchCursor(day_clusters, ["BOAT_NUM","SS_GROUP"]) as sc:
            for row in sc:

                if day_cluster_dict.has_key(row[1]):
                    day_cluster_dict.get(row[1]).append(row[0])
                else:
                    day_cluster_dict[row[1]] = [row[0]]


        cluster_list.append(day_cluster_dict)


    #increment through days in the period: for each day, increment through each SS_GROUP (1-N), for each value_set incrememt
    # the appropriate associates account for each houseboat

    __update_associates(cluster_list, houseboat_list)


    return



def simulate_seastead(houseboat_list):
    """
    For each day, recalculates the position of the boats, based on the move_probability characteristic. In this case,
    discovers clusters only at the end, rather than day by day, which might've been more interesting.
    :param houseboat_list:
    :return:
    """
    #run the days
    for day in range(2, PERIOD+1):
        print("Processing day " + str(day))
        #boat data: dict: key is boat num, value is tuple: [0] move_prob, [1] shape (point tuple, x,y)
        #houseboat_list.dump() -> point,boat_num, move_prob
        #boat_data = get_boat_data(day-1)


        #enumerate the boats in booat data: number is the key of the boat_data
        for boat in range(0, len(houseboat_list)):

            rand_test = random.random()
            production = 0

            #houseboat_list[item].pee() -> tuple: [0]point, [1]move_prob
            boat_data = houseboat_list[boat].pee()

            #test against move_prob
            if rand_test <= boat_data[1]:
                boat_x = WEST + (random.randint(0,LNG_RANGE)/float(1000000))
                boat_y = SOUTH + (random.randint(0,LAT_RANGE)/float(1000000))
                boat_point = arcpy.Point(boat_x, boat_y)
            else:
                boat_point = boat_data[0]
                production = random.randint(0,MAX_PRODUCTION)

            move_prob = random.random()

            houseboat_list[boat].feed(boat_point, day, move_prob, production)
            houseboat_list[boat].add_to_path(boat_point)

            new_boat_day = houseboat_list[boat].poo()


            ic = arcpy.da.InsertCursor(houseboats, ["SHAPE@", "DAY", "BOAT_NUM","BOAT_NAME","MOVE_PROB", "PRODUCTION", "BOAT_X", "BOAT_Y"])
            ic.insertRow(new_boat_day)
            del ic

    discover_clusters(houseboat_list)

    return

def populate_seastead():
    """
    Populate the houseboat feature class with point features with houseboat template attributes.
    :return:
    """

    ic = arcpy.da.InsertCursor(houseboats, ["SHAPE@", "DAY", "BOAT_NUM","BOAT_NAME","MOVE_PROB", "PRODUCTION", "BOAT_X", "BOAT_Y"])
    houseboat_list = []
    for boat in range(0,NUM_BOATS):

        boat_x = WEST + (random.randint(0,LNG_RANGE)/float(1000000))
        boat_y = SOUTH + (random.randint(0,LAT_RANGE)/float(1000000))
        boat_point = arcpy.Point(boat_x, boat_y)

        boat_num = boat + 1
        boat_name = "Boat " + str(boat_num)
        move_prob = random.random()
        while move_prob < MIN_CHANCE or move_prob > MAX_CHANCE:
            move_prob = random.random()
        production = random.randint(0, MAX_PRODUCTION)

        boat_row = [boat_point, 1, boat_num, boat_name, move_prob, production, boat_x, boat_y]

        ic.insertRow(boat_row)

        houseboat_list.append(Houseboat(*boat_row))

    del ic

    return houseboat_list

def main():
    """
    Seasteader is a simple position tracking simulator -- an exercise in arcpy geography objects. The boats don't actually
    interact with each other in the simulation, though Seasteader includes a grouping analysis.
    :return:
    """

    print("Creating feature class...")
    # Process: Create/Recreate new feature class using a template, NAD 1983
    if arcpy.Exists(houseboats):
        arcpy.Delete_management(houseboats)
    arcpy.CreateFeatureclass_management(root, "houseboats", "POINT", template, "DISABLED", "DISABLED", "GEOGCS['GCS_North_American_1983',DATUM['D_North_American_1983',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119521E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")

    print("Populating seastead...")
    houseboat_list = populate_seastead()

    print("Starting simulation...")
    simulate_seastead(houseboat_list)

    generate_paths(houseboat_list)


    return


main()

